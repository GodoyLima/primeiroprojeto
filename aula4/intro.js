console.log("Hello World!");

let variavel = -5;

console.log("variável " + variavel + " é do tipo: " + typeof(variavel));

//operadores matemáticos
let numero1 = 5;
let numero2 = 10;

console.log(""+numero1+""+""+numero2);

console.log(5*2);

let idade = 40;

if (idade == undefined){
    console.log("idade não foi criada !")
}else{
    console.log("Sua idade é: " + idade);
}




function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
        console.log(vlIdade + " - Jovem");
    }else{
        console.log(vlIdade + " - Não muito jovem");
    }
}

mostrarIdade2(42);

const mostraIdade3 = (vlIdade) => {
    if(vlIdade < 42){
        return vlIdade + " - Jovem";
    }else{
         return vlIdade + " - Não muito jovem";
    }
}

mostraIdade3(43);